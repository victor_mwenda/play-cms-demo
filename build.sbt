name := "play-cms-demo"

organization := "ch.insign"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  javaJpa,
  cache,
  "mysql" % "mysql-connector-java" % "5.1.18",
  "org.eclipse.persistence" % "eclipselink" % "2.6.0",
  "com.google.inject" % "guice" % "4.0",
  "javax.inject" % "javax.inject" % "1",
  "commons-lang" % "commons-lang" % "2.6",
  "com.typesafe.akka" %% "akka-actor" % "2.3.11",
  "org.mockito" % "mockito-all" % "1.9.5"
)

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.bintrayRepo("insign", "play-cms")
 )

// fixes problem with loading entities in prod mode, see: https://github.com/playframework/playframework/issues/4590
PlayKeys.externalizeResources := false

// If you want to develop the play-* modules, you need to make next steps:
// 1. Clone all modules to a "modules" folder
//    git clone git@bitbucket.org:insigngmbh/play-cms.git modules/play-cms
//    git clone git@bitbucket.org:insigngmbh/play-auth.git modules/play-auth
//    git clone git@bitbucket.org:insigngmbh/play-shiro.git modules/play-shiro
//    git clone git@bitbucket.org:insigngmbh/play-commons.git modules/play-commons
//    git clone git@bitbucket.org:insigngmbh/play-theme-metronic.git modules/play-theme-metronic
// 2. Comment the play-* library dependencies below
// 3. Uncomment the project definitions below
// 4. Uncomment "dependsOn" and "aggregate" lines from the root definition below
// 5. Comment project root definition in build.sbt of each submodule (last two lines)
// 6. Comment play-* library dependencies in build.sbt of play-cms and play-commons
// 7. Run "rm -rf ~/.ivy2/cache/ch.insign" to be sure the submodules are loaded from the modules folder

libraryDependencies ++= Seq(
  "ch.insign" %% "play-commons" % "1.2.5",
  "ch.insign" %% "play-theme-metronic" % "1.2.5",
  "ch.insign" %% "play-shiro" % "1.2.2",
  "ch.insign" %% "play-auth" % "1.2.6",
  "ch.insign" %% "play-cms" % "1.2.24"
)

 //Project definitions

//lazy val shiro = project.in(file("modules/play-shiro"))
//  .enablePlugins(PlayJava)
//
//lazy val metronic = project.in(file("modules/play-theme-metronic"))
//  .enablePlugins(PlayJava)
//
//lazy val commons = project.in(file("modules/play-commons"))
//  .enablePlugins(PlayJava)
//
//lazy val auth = project.in(file("modules/play-auth"))
//  .enablePlugins(PlayJava)
//  .dependsOn(shiro, commons)
//
//lazy val cms = project.in(file("modules/play-cms"))
//  .enablePlugins(PlayJava)
//  .dependsOn(commons, metronic, shiro, auth)

lazy val root = project.in(file("."))
  .enablePlugins(PlayJava)
//  .dependsOn(cms, metronic, commons, auth, shiro)
//  .aggregate(cms, metronic, commons, auth, shiro)
  .settings(
    Keys.fork in (Test) := false
  )


// Native packaging options

// Enable .deb builds
// See: http://www.scala-sbt.org/sbt-native-packager/formats/debian.html
// TODO: Play needs custom treatment for pid file and application.conf: http://www.scala-sbt.org/sbt-native-packager/topics/play.html
//enablePlugins(DebianPlugin)
//
//name := "play-cms-demo"
//
//version := "1.0-SNAPSHOT"
//
//maintainer := "Martin Bachmann <m.bachmann@insign.ch>"
//
//packageSummary := "The play-cms demo application."
//
//packageDescription := """A package description of our software,
//  with multiple lines."""
//
//
//// Settings for pid file and application.conf
//// See: http://www.scala-sbt.org/sbt-native-packager/topics/play.html
//javaOptions in Universal ++= Seq(
//  // JVM memory tuning
//  "-J-Xmx1024m",
//  "-J-Xms512m",
//
//  // Since play uses separate pidfile we have to provide it with a proper path
//  s"-Dpidfile.path=/var/run/${packageName.value}/play.pid",
//
//  // Use separate configuration file for production environment
//  s"-Dconfig.file=/usr/share/${packageName.value}/conf/production.conf",
//
//  // Use separate logger configuration file for production environment
//  s"-Dlogger.file=/usr/share/${packageName.value}/conf/production-logger.xml"
//)


// Enable docker builds
// Build using: sbt docker:publishLocal
// See: http://www.scala-sbt.org/sbt-native-packager/formats/docker.html or https://github.com/muuki88/sbt-native-packager-examples/tree/master/play-2.3

enablePlugins(DockerPlugin)

maintainer in Docker := "Martin Bachmann <m.bachmann@insign.ch>"

packageSummary in Docker := "The play-cms demo application."

aggregate in Docker := false

dockerRepository := Some("insign-docker-registry.bintray.io")
