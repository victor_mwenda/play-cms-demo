import ch.insign.commons.db.JPA;
import ch.insign.playshiro.mgt.PlayShiroSecurityManager;
import ch.insign.playshiro.subject.PlayShiroSubject;
import ch.insign.playshiro.util.PluginUtils;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.apache.shiro.util.ThreadState;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import play.mvc.Http;
import play.test.FakeApplication;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static play.test.Helpers.*;

public class AbstractTest {

    protected static EntityManager em;
    protected static FakeApplication app;

    /**
     * Create and Start FakeApplication and bind default JPA-Entity-Manager for current Thread
     */
    @BeforeClass
    public static void setUp() {
        app = fakeApplication(inMemoryDatabase(), fakeGlobal());
        start(app);

        bindEntityManager();

    }

    public static void bindEntityManager() {
        em = JPA.em("default");
        JPA.bindForCurrentThread(em);
    }

    /**
     * Setup a Shiro Thread State. Necessary to test authentication
     */
    public static void setupShiroThreadState() {
        createFakeHttpContext();
        PlayShiroSecurityManager securityManager = PluginUtils.getPlayShiroEnvironment().getPlayShiroSecurityManager();
        PlayShiroSubject subject = new PlayShiroSubject.Builder(securityManager, Http.Context.current())
            .buildPlayShiroSubject();


        ThreadState threadState = new SubjectThreadState(subject);
        threadState.bind();
    }


    /**
     * Create a fake http context which allows to access Http.context / Controller.ctx() from tests
     * Will return "testhost" when request.host() is accessed.
     *
     */
    public static void createFakeHttpContext() {
        try {
            Http.Context.current();
        } catch(Exception e) {
            // Create a mock http context  using Mockito
            Http.Request request = mock(Http.Request.class);
	        when(request.host()).thenReturn("testhost");
	        when(request.queryString()).thenReturn(new HashMap<>());
	        when(request.method()).thenReturn("GET");
	        // TODO: Add more mock returns as needed

            Map<String, String> flashData = Collections.emptyMap();
            Map<String, Object> argData = Collections.emptyMap();
            // FIXME: argData.put("currentEntityManager", JPA.em("default")); // <-- ugly, but needed by JPA.em(), otherwise it cries "No EntityManager bound to this thread"
            play.api.mvc.RequestHeader header = mock(play.api.mvc.RequestHeader.class);
            Http.Context context = new Http.Context(2L, header, request, flashData, flashData, argData);
            Http.Context.current.set(context);
        }
    }

    /**
     * Start new DB-Transaction
     */
    protected void t_begin() {
        em.getTransaction().begin();
    }

    /**
     * Commit current DB-Transaction and execute clear on EntityManager to detach current Entities
     * This ensures that all previously committed changes are persisted to the DB.
     * To work with the same entities after t_commitAndClear(), they have to be fetched again from DB
     */
    protected void t_commitAndClear() {
        em.getTransaction().commit();
        em.clear();
    }

    /**
     * Remove Entity-Manager from current Thread and close it
     */
    @AfterClass
    public static void tearDown() {
        JPA.bindForCurrentThread(null);
        if(em.isOpen()) {
            em.close();
        }
        stop(app);

    }
}
