import java.util.UUID;

public class DummyBean {

	public DummyBean() {
	}

	public String getId() {
		return UUID.randomUUID().toString();
	}
}