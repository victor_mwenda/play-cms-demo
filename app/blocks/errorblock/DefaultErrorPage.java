package blocks.errorblock;

import ch.insign.cms.models.CMS;
import play.twirl.api.Html;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import ch.insign.cms.blocks.errorblock.ErrorPage;

@Entity
@Table(name = "default_error_page")
@DiscriminatorValue("DefaultErrorPage")
public class DefaultErrorPage extends ErrorPage {
    public static final String COLLECTION_SLOT = "slot1";

    @Override
    public Html render() {
        String content = CMS.getFilterManager().processOutput(
                blocks.errorblock.html.errorShow.render(this).toString(), null
        );
        return Html.apply(content);
    }
}
