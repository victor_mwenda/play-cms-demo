package blocks.teaserblock;

import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.ContentBlock;
import ch.insign.commons.db.MString;
import ch.insign.commons.i18n.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.*;

@Entity
@Table(name = "block_teaser")
@DiscriminatorValue("TeaserContentBlock")
public class TeaserBlock extends ContentBlock {
	private final static Logger logger = LoggerFactory.getLogger(TeaserBlock.class);

    @MString.MStringRequired
    @OneToOne(cascade= CascadeType.ALL)
    private MString subtitle = new MString();

    @MString.MStringRequired
    @OneToOne(cascade= CascadeType.ALL)
    private MString logoUrl = new MString();

    @MString.MStringRequired
    @OneToOne(cascade= CascadeType.ALL)
    private MString linkText = new MString();

    @MString.MStringRequired
    @OneToOne(cascade= CascadeType.ALL)
    private MString linkUrl = new MString();

    public MString getLogoUrl() {
        return this.logoUrl;
    }

    public void setLogoUrl(MString logoUrl) {
        this.logoUrl = removeHttpFromUrl(logoUrl);
    }

    public MString getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(MString subtitle) {
        this.subtitle = subtitle;
    }

    public MString getLinkText() {
        return linkText;
    }

    public void setLinkText(MString linkText) {
        this.linkText = linkText;
    }

    public MString getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(MString linkUrl) {
        this.linkUrl = removeHttpFromUrl(linkUrl);
    }

    public static BlockFinder<TeaserBlock> find = new BlockFinder<>(TeaserBlock.class);

    @Override
    public Html render() {
        return blocks.teaserblock.html.teaserBlock.render(this);
    }

	@Override
	public Html editForm(Form editForm) {
		String backURL = Controller.request().getQueryString("backURL");
		return blocks.teaserblock.html.teaserBlockEdit.render(this, editForm, backURL, null);
	}

    private MString removeHttpFromUrl(MString url) {
        for (String lang : Language.getAllLanguages()) {
            String newUrl = url.get(lang).replaceAll("(http://|https://)", "");
            url.set(lang, newUrl);
        }
        return url;
    }
}
