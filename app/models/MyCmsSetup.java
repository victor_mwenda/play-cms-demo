package models;

import blocks.pageblock.DefaultPageBlock;
import blocks.teaserblock.TeaserBlock;
import blocks.errorblock.DefaultErrorPage;
import ch.insign.cms.models.*;
import ch.insign.cms.service.EmailService;
import ch.insign.commons.db.MString;
import play.twirl.api.Html;
import widgets.registeredusers.RegisteredUsersWidget;
import ch.insign.cms.blocks.errorblock.ErrorPage;
import views.html.email_layout;

public class MyCmsSetup  extends Setup {
    private static final String ERROR_500 = "500 Application error";
    private static final String ERROR_EXPIRED = "404 page not found";
    private static final String ERROR_404 = "404 page not found";
    private static final String ERROR_FORBIDDEN = "403 page forbidden";

    @Override
    public void registerPartyFormManager() {

        // register party form manager to specify user's custom edit and create forms
        CMS.setPartyFormManager(new MyUserHandler());
    }

    @Override
    public void registerPartyEventHandler() {

        // register party event handler handles party's CRUD events
        CMS.setPartyEvents(new MyUserHandler());
    }

    @Override
    public void registerBlocks() {
        super.registerBlocks();
        CMS.getBlockManager().register(
                DefaultPageBlock.class,
                TeaserBlock.class,
                DefaultErrorPage.class
        );
    }

    @Override
    public void registerContentFilters() {
        super.registerContentFilters();
        CMS.getFilterManager().register(
                new RegisteredUsersWidget()
        );
    }

    @Override
    public void registerEmailService() {
        // Override render method of default EmailService to set custom layout for mails
        CMS.registerEmailService(new EmailService() {
            @Override
            protected Html render(String content, String language) {
                return email_layout.render(content);
            }
        });
    }

    @Override
    public void addExampleData() {
        super.addExampleData();
        createTeaserBlocks();
        createWidgetExampleBlock();
        createErrorTemplates();
    }

    @Override
    public ErrorPage getErrorPageInstance() {
        return new DefaultErrorPage();
    }

    @Override
    public ErrorPage createErrorPageNotFound() {
        ErrorPage errorPage = super.createErrorPageNotFound();
        createErrorPageContent(errorPage, ERROR_404);
        return errorPage;
    }

    @Override
    public ErrorPage createErrorPageUnavailable() {
        ErrorPage errorPage =  super.createErrorPageUnavailable();
        createErrorPageContent(errorPage, ERROR_EXPIRED);
        return errorPage;
    }

    @Override
    public ErrorPage createErrorPageInternalServerError() {
        ErrorPage errorPage = super.createErrorPageInternalServerError();
        createErrorPageContent(errorPage, ERROR_500);
        return errorPage;
    }

    @Override
    public ErrorPage createErrorPageForbidden() {
        ErrorPage errorPage = super.createErrorPageForbidden();
        createErrorPageContent(errorPage, ERROR_FORBIDDEN);
        return errorPage;
    }

    /**
     * Override helper method to create example pages of DemoPageBlock class
     */
    @Override
    protected PageBlock createPage(String key, PageBlock parent, String vpathEN) {
        DefaultPageBlock page = new DefaultPageBlock();
        page.setKey(key);
        if (parent != null) {
            parent.getSubBlocks().add(page);
            page.setParentBlock(parent);
        }
        page.save();

        createNavItem(page, vpathEN, "en");
        return page;
    }

    private void createWidgetExampleBlock() {
        PageBlock homepage = (PageBlock) AbstractBlock.find.byKey(KEY_HOMEPAGE);
        try {
            CollectionBlock sidebar = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "sidebar");
            ContentBlock contentBlock = (ContentBlock)sidebar.addSubBlock(ContentBlock.class);
            contentBlock.getTitle().set("en", "Widgets");
            contentBlock.getContent().set("en",
                    "<p>Content filters are a great tool to let the user add variables freely inside any content, " +
                    "e.g. [[currentUserCount]] which your filter resolves to the current value " +
                    "when displaying the page.</p>\n" +
                    "<h4>Content filter example.</h4>\n" +
                    "<p>Last registered users: [[registeredUsersWidget:5]]</p>\n" +
                    "<p><a href=\"https://confluence.insign.ch/display/PLAY/Play+CMS\">" +
                    "Learn more </a> about filter framework</p>");
            contentBlock.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createTeaserBlocks() {
        try {
            PageBlock homepage = (PageBlock) AbstractBlock.find.byKey(KEY_HOMEPAGE);
            CollectionBlock bottomPane = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "bottom");
            TeaserBlock block, block2, block3;
            block = (TeaserBlock)bottomPane.addSubBlock(TeaserBlock.class);
            block.getTitle().set("en", "Teaser block");
            block.getSubtitle().set("en", "Lorep Ipsum");
            block.getContent().set("en", "This is an example of customizing cms content blocks");
            block.getLogoUrl().set("en", "/assets/images/yacht.png");
            block.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block.getLinkText().set("en", "Learn more");

            block2 = (TeaserBlock)bottomPane.addSubBlock(TeaserBlock.class);
            block2.getTitle().set("en", "Teaser block");
            block2.getSubtitle().set("en", "Lorep Ipsum");
            block2.getContent().set("en", "This is an example of customizing cms content blocks");
            block2.getLogoUrl().set("en", "/assets/images/yacht2.jpg");
            block2.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block2.getLinkText().set("en", "Learn more");

            block3 = (TeaserBlock) bottomPane.addSubBlock(TeaserBlock.class);
            block3.getTitle().set("en", "Teaser block");
            block3.getSubtitle().set("en", "Lorep Ipsum");
            block3.getContent().set("en", "This is an example of customizing cms content blocks");
            block3.getLogoUrl().set("en", "/assets/images/yacht3.jpg");
            block3.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block3.getLinkText().set("en", "Learn more");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createErrorPageContent (ErrorPage errorPage, String errorMsg) {
        CollectionBlock pane = (CollectionBlock) Template.addBlockToSlot(
                CollectionBlock.class,
                errorPage,
                DefaultErrorPage.COLLECTION_SLOT
        );

        ContentBlock errorBlock = null;
        try {
            errorBlock = (ContentBlock)pane.addSubBlock(ContentBlock.class);
            pane.save();
            // Set the Error Text in Default Language
            MString contentString = new MString();
            contentString.set("en", errorMsg);

            errorBlock.setContent(contentString);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        errorBlock.save();
        pane.save();
        errorPage.save();
    }

    /**
     * Create an EmailTemplate for sending email to user after password reset
     */
    private void createErrorTemplates() {
        EmailTemplate template = new EmailTemplate();
        template.setTemplateKey("password.recovery.success");
        template.setSender("admin@localhost");
        template.setCategory(EmailTemplate.EmailTemplateCategory.EXTERN);
        template.getContent().set("en", "Hello {firstname} {lastname}. A new password for your account has been created. ");
        template.getContent().set("de", "Hello {firstname} {lastname}. A new password for your account has been created. ");
        template.getDescription().set("en", "New password has been created.");
        template.getDescription().set("en", "New password has been created.");
        template.getSubject().set("en", "New password has been created.");
        template.getSubject().set("de", "New password has been created.");
        template.save();
    }
}
