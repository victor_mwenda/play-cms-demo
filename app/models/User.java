package models;


import ch.insign.playauth.party.support.DefaultParty;

import javax.persistence.Entity;

/**
 * Demo user class extends DefaultParty with custom fields
 */
@Entity
public class User extends DefaultParty {

    public String lastName;
    public String phone;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
