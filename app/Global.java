import ch.insign.cms.models.CMS;
import ch.insign.commons.db.MString;
import ch.insign.commons.db.MStringDataBinder;
import models.MyAuthSetup;
import models.MyCmsSetup;
import play.Application;
import play.GlobalSettings;


import static ch.insign.commons.db.DataBinding.registerDataBinder;

public class Global extends GlobalSettings {

    static {
        registerDataBinder(MString.class, new MStringDataBinder());
    }

    @Override
    public void onStart(Application application) {

            // Set default AuthSetup class which initialize default roles and parties on first run.
            CMS.setAuthSetup(new MyAuthSetup());
            // CMS app start setup:
            // Set our own config/setup instances
            CMS.setSetup(new MyCmsSetup());
            CMS.getSetup().onAppStart();
    }
}
