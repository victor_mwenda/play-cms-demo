#!/usr/bin/env bash

# 1. Get UID:GID of working dir
# 2. Put UID and GID as environment vars to a common.env file
# 3. entrypoint.sh creates a new user with this UID and GID and run sbt with this user

USER_ID=`stat -c "%u" .`
GROUP_ID=`stat -c "%g" .`

echo "USER_ID=$USER_ID" > common.env
echo "GROUP_ID=$GROUP_ID" >> common.env

# use 'run' to return a proper exit code of app
docker-compose -f test.yml run --rm app
