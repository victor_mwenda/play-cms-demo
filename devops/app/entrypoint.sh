#!/usr/bin/env bash

set -e

if [ "$1" = 'sbt' ] && [ -n "$USER_ID" ]; then
    usermod -u $USER_ID sbt
    chown -R sbt /usr/src/app/source
    chown -R sbt /home/sbt
    exec gosu sbt "$@"
else
    exec "$@"
fi